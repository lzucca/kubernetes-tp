# Clonar el repositorio
# Ingresar al directorio clonado y seguir los pasos.

# Creamos, en el host, un directorio donde va se va a almacenar el volumen persistente

sudo mkdir -p /mnt/drupal

# Creamos el volumen
kubectl apply -f drupal-pv.yaml

# Chequeamos que el volumen se haya creado correctamente
kubectl get pv

# Creamos el reclamo de espacio en el volumen
kubectl apply -f drupal-claim.yaml

# Chequeamos que el reclamo se haya creado correctamente
kubectl get pvc

# Lanzamos el deployment
kubectl apply -f drupal-deployment.yaml

# Chequeamos que el deployment se haya levantado correctamente
kubectl get deployments

# Chequeamos que los pods esten corriendo
kubectl get pods

# Ejecutamos el forwarding de puerto hacia el deployment
kubectl port-forward deployment/drupal-deployment 8080:80 --address 0.0.0.0


# Ingresamos desde el navegador web con la direccion ip del host y apuntando al puerto 8080 (xxx.xxx.xxx.xxx:8080)
# Elegimos idioma (Preferentemente "English")
# Installation profile "Demo"
# Database type: "SQLlite"


# Al terminar la instalacion podremos ver la web de demostracion.
